import java.awt.Color;
public class Zoo{
	public static void main (String args[]){
		Lion singa = new Lion ();
		Horse kuda = new Horse ();
		Kangoroo kangguru = new Kangoroo ();
 		
		singa.setWarnabulu(new Color (0, 1, 1));
		singa.setNama("Mufasa");
		singa.setUsia(10);
		singa.setBb(170);
		singa.diadopsi("Ragunan");
		
		kuda.setWarnabulu(new Color (0, 1, 1));
		kuda.setNama("Pegasus");
		kuda.setUsia(20);
		kuda.setBb(440);
		kuda.diadopsi("Ragunan");

		kangguru.setWarnabulu(new Color (0, 1, 1));
		kangguru.setNama("Ausie");
		kangguru.setUsia(11);
		kangguru.setBb(80);
		kangguru.diadopsi("Ragunan");
		
		System.out.println("----Lion----");
		singa.cetakInformasi();
		
		System.out.println("");
		System.out.println("----Horse----");
		kuda.cetakInformasi();
		
		System.out.println("");
		System.out.println("----Kangoroo----");
		kangguru.cetakInformasi();
	}
}
