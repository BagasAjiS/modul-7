import java.awt.Color;
public class LingkunganKucing{
	public static void main(String[] args){
		Kucing michael = new Kucing();
		Kucing garfield = new Kucing();

		//michael
		System.out.println( );
		michael.WarnaBulu = new Color(10,10,2);
		michael.nama = "Michael";
		michael.usia = 8;
		michael.bb = 4.5;
		michael.diadopsi("Rezki");
		michael.cetakInformasi();
		
		//garfield
		System.out.println( );
		garfield.WarnaBulu = new Color(10,10,2);
		garfield.nama = "garfield";
		garfield.usia = 3;
		garfield.bb = 2.1;
		garfield.diadopsi("James");
		garfield.cetakInformasi();
		
	}
}